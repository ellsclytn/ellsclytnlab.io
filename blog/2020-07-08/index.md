---
date: '2020-07-08'
title: 'Fingerprint unlocking on a Linux laptop'
tags: ['linux', 'systemd', 'thinkpad']
---

If you're running Arch Linux on a laptop with a supported finerprint reader, you might like to be able to use your fingerprint to unlock after suspend. At first glance of the [Arch Wiki `fprint` page](https://wiki.archlinux.org/index.php/Fprint), it would appear you can simply add some config into `/etc/pam.d/i3lock` and you'd be good to go. However, the common approach of using a Systemd unit file with `Before=sleep.target` to start a lock application (`i3lock`, in my case) won't work, and you'll find the lock application rejecting your fingerprint every time.

This is due to the fact that `sleep.target` is for _system_ services, as opposed to user services. Further, there's no direct equivalent for `systemd-user`. So when the suspend hook runs, it might run with _some_ knowledge of an active session (if you're passing through a `DISPLAY` environment variable, for example), but it won't have a full user session to work with, which means no fingerprint reading.

Fortunately, this is pretty easily fixed. The [`xss-lock`](https://www.archlinux.org/packages/community/x86_64/xss-lock/) application allows you to run a locker application as a screensaver, which means full user session availability to the locker. To integrate this with your existing locker, the steps are:

1. Disable any systemd lock-on-suspend services you might have previously been trying to use.
2. Invoke `xss-lock` with your X11 session. For example, with i3, something like `exec --no-startup-id xss-lock -l /bin/my-locker-script &` would do the trick
3. Restart your Window Manger (or X11 entirely, depending on how you've configured `xss-lock` to be executed)
4. Enjoy!

## Resources

- [Unix Stack Exchange - _How to run systemd user service to trigger on sleep (aka. suspend, hibernate)?_](https://unix.stackexchange.com/questions/149959/how-to-run-systemd-user-service-to-trigger-on-sleep-aka-suspend-hibernate)
