import React, { memo } from 'react'
import styled from 'styled-components'
import { media } from '../utils/media'

const HeaderWrapper = styled.header`
  position: relative;
  background: ${({ theme }) => theme.colors.base[1]};
  padding: 0 2rem 10rem;
  text-align: center;
  @media ${media.tablet} {
    padding: 4rem 2rem 6rem;
  }
  @media ${media.phone} {
    padding: 1rem 0.5rem 2rem;
  }
`

const Content = styled.div`
  position: relative;
`

export const Header: React.FC = memo(({ children }) => (
  <HeaderWrapper>
    <Content>{children}</Content>
  </HeaderWrapper>
))
