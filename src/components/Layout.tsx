import React, { memo } from 'react'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import theme from '../../config/Theme'
import { media } from '../utils/media'
import './layout.scss'
import { Nav } from './'

const GlobalStyle = createGlobalStyle`
  ::selection {
    color: ${theme.colors.bg};
    background: ${theme.colors.primary};
  }
  body {
    background: ${theme.colors.bg};
    color: ${theme.colors.base[3]};
  }
  a {
    color: ${theme.colors.base[2]};
    text-decoration: underline;
    transition: all ${theme.transitions.normal};
  }
  a:hover {
    color: ${theme.colors.base[4]};
  }
  h1, h2, h3, h4 {
    color: ${theme.colors.base[3]};
    font-weight: 600;

    a {
    text-decoration: none;
    }
  }
  blockquote {
    font-style: italic;
    position: relative;
  }

  blockquote:before {
    content: "";
    position: absolute;
    background: ${theme.colors.primary};
    height: 100%;
    width: 6px;
    margin-left: -1.6rem;
  }
  label {
    margin-bottom: .5rem;
    color: ${theme.colors.grey.dark};
  }
  input, textarea {
    border-radius: .5rem;
    border: none;
    background: rgba(0, 0, 0, 0.05);
    padding: .25rem 1rem;
    &:focus {
      outline: none;
    }
  }
`

interface Props {
  navVisible?: boolean
}

export const Layout: React.FC<Props> = memo(
  ({ children, navVisible = true }) => (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        {navVisible && <Nav />}
        {children}
      </>
    </ThemeProvider>
  )
)
