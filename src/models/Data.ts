import type AllMarkdownRemark from './AllMarkdownRemark'

interface Data {
  allMarkdownRemark: AllMarkdownRemark
}

export default Data
