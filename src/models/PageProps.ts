import type PageContext from './PageContext'
import type PageResources from './PageResources'
import type Data from './Data'

interface PageProps {
  data: Data
  location: Location
  pageResources?: PageResources
  pageContext: PageContext
}

export default PageProps
