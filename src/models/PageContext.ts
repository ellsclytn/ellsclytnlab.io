import type Post from './Post'

interface PageContext {
  tags?: string[]
  tagName?: string
  posts?: Post[]
  next: any
  prev: any
}

export default PageContext
