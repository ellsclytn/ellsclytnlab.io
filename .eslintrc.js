module.exports = {
  env: {
    es2021: true,
  },
  extends: ['standard-with-typescript', 'plugin:react/recommended'],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: './tsconfig.json',
  },
  overrides: [
    {
      files: ['*.js', '*.jsx', '*.ts', '*.tsx'],
      extends: [
        'standard-with-typescript',
        'plugin:react/recommended',
        'standard-jsx',
      ],
    },
  ],
  settings: {
    react: {
      version: 'detect',
    },
  },
  plugins: ['react'],
  rules: {},
}
